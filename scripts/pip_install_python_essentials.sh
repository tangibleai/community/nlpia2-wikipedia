pip install --upgrade \
  virtualenv \ 
  build \
  twine \
  pip \
  black \
  pytest \
  python-lsp-black \
  spyder \
  poetry \
  wheel
